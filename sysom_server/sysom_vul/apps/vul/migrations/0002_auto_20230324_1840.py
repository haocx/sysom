# Generated by Django 3.2.16 on 2023-03-24 10:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vul', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='safixhisttohost',
            name='host',
        ),
        migrations.RemoveField(
            model_name='securityadvisoryfixhistorymodel',
            name='host',
        ),
        migrations.RemoveField(
            model_name='securityadvisorymodel',
            name='host',
        ),
        migrations.RemoveField(
            model_name='vulmodel',
            name='host',
        ),
        migrations.AddField(
            model_name='safixhisttohost',
            name='hosts',
            field=models.TextField(default='', verbose_name='关联主机'),
        ),
        migrations.AddField(
            model_name='securityadvisoryfixhistorymodel',
            name='hosts',
            field=models.TextField(default='', verbose_name='关联主机'),
        ),
        migrations.AddField(
            model_name='securityadvisorymodel',
            name='hosts',
            field=models.TextField(default='', verbose_name='关联主机'),
        ),
        migrations.AddField(
            model_name='vulmodel',
            name='hosts',
            field=models.TextField(default='', verbose_name='关联主机'),
        ),
    ]
