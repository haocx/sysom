import React, { useRef } from "react";
import ProTable from "@ant-design/pro-table";
import { getTaskList } from "../../service";
import { FormattedMessage } from 'umi';

import { Button } from "antd";

const getCPUTaskList = async () => {
  return await getTaskList({ service_name: "loadtask" });
}

const DiagnoTableList = React.forwardRef((props, ref) => {

  const columns = [
    {
      title: <FormattedMessage id="pages.diagnose.instanceIP" defaultMessage="Instance IP" />,
      dataIndex: "instance",
      valueType: "textarea"
    },
    {
      title: <FormattedMessage id="pages.clusterTable.clusterCreatedAt" defaultMessage="Created Time" />,
      sortOrder: "descend",
      dataIndex: "created_at",
      valueType: "dateTime",
    },
    {
      title: <FormattedMessage id="pages.diagnose.diagnosticID" defaultMessage="Diagnostic ID" />,
      dataIndex: "task_id",
      valueType: "textarea",
    },
    {
      title: <FormattedMessage id="pages.diagnose.state" defaultMessage="State" />,
      dataIndex: 'status',
      valueEnum: {
        Running: { text: <FormattedMessage id="pages.hostTable.status.running" defaultMessage="Running" />, status: 'Processing' },
        Success: { text: <FormattedMessage id="pages.diagnose.completediagnosis" defaultMessage="Complete diagnosis" />, status: 'Success' },
        Fail: { text: <FormattedMessage id="pages.hostTable.status.abnormal" defaultMessage="Abnormal" />, status: 'Error' },
      },
    },
    {
      title: <FormattedMessage id="pages.diagnose.operation" defaultMessage="Operation" />,
      dataIndex: "option",
      valueType: "option",
      render: (_, record) => {
        if (record.status == "Success") {
          return (
            <a key="showDiagnose" onClick={() => {
              props?.onClick?.(record)
            }}><FormattedMessage id="pages.diagnose.viewdiagnosisresults" defaultMessage="Viewing diagnosis results" /></a>
          )
        }
        else if (record.status == "Fail") {
          return (
            <a key="showError" onClick={() => {
              props?.onError?.(record)
            }}><FormattedMessage id="pages.diagnose.viewerrormessages" defaultMessage="Viewing error messages" /></a>
          )
        }
        else {
          return (<span><FormattedMessage id="pages.diagnose.nooperation" defaultMessage="No operation is available for the time being" /></span>);
        }
      },
    }
  ];

//     pagination={{ pageSize: 5 }}
  return (
    <ProTable
      headerTitle={props.headerTitle}
      actionRef={ref}
      params={props.params}
      rowKey="id"
      request={getCPUTaskList}
      columns={columns}
      pagination={props.pagination}
 
      search={false}
    />
  );
});

export default DiagnoTableList;
