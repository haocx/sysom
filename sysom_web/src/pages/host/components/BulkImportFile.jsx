import { Button, Alert, notification, Modal, Table } from "antd";
import {
    CheckCircleOutlined,
    CloseCircleOutlined
} from '@ant-design/icons';
import { ModalForm, ProFormUploadButton, ProFormText } from "@ant-design/pro-form";
import { FormattedMessage } from 'umi';
import { useRef } from 'react'
import { scpFileToHost } from '../service'

const Tip = () => {
    return <>
        <Alert message="上传文件不会存储至后端服务" type="warning" closable showIcon style={{ marginBottom: '10px' }} />
        <Alert message="如果目录不存在将会上传失败，后端服务不会对目录是否存在做校验。" type="warning" closable showIcon style={{ marginBottom: '10px' }} />
    </>
}

const BulkResult = (props) => {
    const columns = [
        {
            title: '主机IP',
            dataIndex: 'instance',
            key: 'instance',
            align: 'center',
            width: 110,
        }, {
            title: '是否成功',
            dataIndex: 'success',
            align: 'center',
            width: 90,
            render: (bool) => {
                return (
                    bool ? <CheckCircleOutlined style={{'color': "#4DAC1E"}} /> : <CloseCircleOutlined style={{'color': "#A82229"}} />
                )
            },
        }, {
            title: '提示信息',
            dataIndex: 'err_msg',
            ellipsis: true,
            align: 'center',
            width: 200,
            render: (text) => {
                return text? text: 'Success'
            }
        }
    ]
    return <Table columns={columns} dataSource={props.data} pagination={false} />
}

const BulkImportFile = (props) => {
    const BulkPushForm = useRef()
    const BulkImportFileEvent = async (values) => {
        let status = true;
        const formData = new FormData();
        formData.append('opt', 'send-file')
        if (props.bulk) {
            formData.append('remote_nodes', props.selectedRowKeys)
        } else {
            formData.append('remote_nodes', [props.record.id])
        }
        formData.append('file', values.file[0].originFileObj)
        formData.append('remomte_path', values.dir)
        const token = localStorage.getItem('token')
        await scpFileToHost(formData, token).then((res) => {
            if (props.bulk) {
                props.onCleanSelected();
                Modal.info({
                    title: '推送结果',
                    content: (<BulkResult data={res.data} />),
                    okText: '知道了',
                    width: 520
                })
            } else {
                res.data?.map((item) => {
                    if (item.success) {
                        notification.success({
                            message: `${item.instance} push success!`
                        })
                    } else {
                        notification.error({
                            message: `${item.instance} push error! ${item.err_msg}}`
                        })
                    }
                })
            }
        }).catch((err) => {
            let status = false
        })


        BulkPushForm?.current?.setFieldValue();
        return status;
    }

    return (
        <ModalForm
            title="上传文件"
            formRef={BulkPushForm}
            width="440px"
            trigger={
                <Button type="link">
                    <FormattedMessage id="pages.hostTable.batchimportfile" defaultMessage="batchimportfile" />
                </Button>
            }
            onFinish={async (values) => {
                return await BulkImportFileEvent(values)
            }}
        >
            <Tip />
            <ProFormUploadButton
                name="file"
                fieldProps={{
                    beforeUpload: () => { return false }
                }}
            />
            <ProFormText
                name="dir"
                placeholder="请输入需要上传至节点位置"
                label="目录"
                rules={[{ required: true, message: "upload dir required!" }]}
            />
        </ModalForm>
    )
}

export default BulkImportFile

