import WebConsole from '@/components/WebConsole';
import { useIntl } from 'umi';

const Terminal = (props) => {
  const intl = useIntl();
  const userId = localStorage.getItem('userId');
  return (
    <WebConsole title={intl.formatMessage({
      id: 'pages.hostTable.Onlineterminal',
      defaultMessage: 'On-line terminal',
    })} host_ip={props.match.params.ip}  user_id={userId} />
  )
};

export default Terminal